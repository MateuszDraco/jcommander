package com.mdraco.jcommander.ui;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * User: mateusz
 * Date: 08.06.2013
 * Time: 08:50
 * Created with IntelliJ IDEA.
 */
public class FileModel extends AbstractTableModel {
	private ArrayList<File> files;
	private String[] columns = {"name", "size"};

	public FileModel(File file) {
		files = new ArrayList<File>();
		updateData(file);
	}

	public void updateData(File file) {
		files.clear();
		Collections.addAll(files, file.listFiles());

		files.add(new File(".."));

		Collections.sort(files, new Comparator<File>() {
			@Override
			public int compare(File file1, File file2) {
				if ((file1.isDirectory() && file2.isDirectory()) || (file1.isFile() && file2.isFile()))
					return file1.getName().compareTo(file2.getName());
				if (file1.isFile())
					return 1;
				return -1;  //To change body of implemented methods use File | Settings | File Templates.
			}
		});
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return files.size();
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public String getColumnName(int i)
	{
		return columns[i];
	}

	@Override
	public Object getValueAt(int i, int i2) {
		File file = files.get(i);
		switch (i2) {
			case 0:
				return file.getName();
			case 1:
				if (file.isDirectory())
					return String.format("%d files", file.listFiles().length);
				else if (file.isFile())
					return String.format("%d bytes", file.length());
		}
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}

	public File getFile(int row) {
		if (row < 0 || row >= files.size())
			return null;
		return files.get(row);
	}
}
