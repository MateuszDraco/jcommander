package com.mdraco.jcommander.ui;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

/**
 * User: mateusz
 * Date: 08.06.2013
 * Time: 07:53
 * Created with IntelliJ IDEA.
 */
public class MainWindow extends JFrame {
	/**
	 * Constructor
	 */
	public MainWindow() {
		super("JCommander");

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(400, 200);
		setLayout(new BorderLayout(20, 10));
		setJMenuBar(createMenu());

		add(createTable(), BorderLayout.CENTER);

		setVisible(true);
	}

	private JComponent createTable() {
		JTable table = new JTable(new FileModel(new File(".")));

		table.addMouseListener(new MouseInputAdapter() {
			public JTable table;

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();

					int row = table.getSelectedRow();
					FileModel model = (FileModel) table.getModel();
					if (model != null) {
						File file = model.getFile(row);
						if (file != null) {
							model.updateData(file);
						}
					}

				}
			}

			public MouseAdapter init(JTable table) {
				this.table = table;
				return this;
			}
		}.init(table));

		JScrollPane pane = new JScrollPane(table);
		table.setFillsViewportHeight(true);

		return pane;
	}

	private JMenuBar createMenu() {
		JMenuBar bar = new JMenuBar();

		JMenu menu = new JMenu("File");
		bar.add(menu);

		JMenuItem item = new JMenuItem("Exit", KeyEvent.VK_X);
		menu.add(item);
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				processWindowEvent(new WindowEvent(MainWindow.this, WindowEvent.WINDOW_CLOSING));
			}
		});

		return bar;
	}
}
